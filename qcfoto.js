    var gst = '';
    var imglst = [];
    var favorites = [];
    var favcount = 0;
    var qry, searchbox, view, getval, overlay, request, favrequest, none, closeit, requestFive, requestTT, handle, insp, current, expires, favbutton, loginbox, user, pass, signin, logreq, logresp, srvresp;

    function $ (id) {
        return document.getElementById(id);
    };

    // From CSS-Tricks.com
    function shuffle (o) {
        for (var j, x, i = o.length; i; j = parseInt(Math.random() * i, 10), x = o[--i], o[i] = o[j], o[j] = x);
        return o;
    };


     function cookiecheck () {
        if (document.cookie) {
            favorites = JSON.parse(document.cookie.split("favoriteslist=")[1]);
            if (favorites.length > 0) {
                favrequest.style.display = 'inline-block';
                favbutton.style.display = 'block';
            }
            else {
                favrequest.style.display = 'none';
                favbutton.style.display = 'none';
            }
        }
    };


    function processing () {
        clearTimeout(handle);
        none.innerHTML = "Processing";
        none.style.display = "block";
        var inc = ".";
        var count = 0;
        handle = setInterval(

     function () {
         if (count < 3) {
             none.innerHTML += inc;
             count++;
         } else {
             none.innerHTML = "Processing";
             count = 0;
         }
     }, 1000);
    };

    function setText () {
        getval.innerHTML = "Search Again";
        closeit[0].innerHTML = "[ Close Panel ]";
        closeit[1].innerHTML = "[ Close Panel ]";
        var w = window.innerWidth || document.documentElement.clientWidth || document.body.clientWidth;
        if (w > 800) {
            getval.innerHTML += " (F Key)";
            closeit[0].innerHTML = "[ Close Panel  (Ctrl + C) ]";
            closeit[1].innerHTML = "[ Close Panel  (Ctrl + C) ]";
            overlay.style.backgroundSize = "auto";
        } else if (w < 800) {
            overlay.style.backgroundSize = "contain";
        }
    };

    function addFav (info) {
        if (info.className !== "favorited") {
            obj = {
                thumb: info.getAttribute("data-thumb"),
                large: info.getAttribute("data-large"),
                title: info.getAttribute("data-title"),
                username: info.getAttribute("data-username"),
                source: info.getAttribute("data-source"),
                index: favorites.length
            };
            favcount++;
            info.innerHTML = "Added to Favorites.";
            info.className = info.className + "favorited";
            favorites.push(obj);
            document.cookie = "favoriteslist=" + JSON.stringify(favorites) + ";expires=Tue, 31 Dec 2030 12:00:00 GMT";
            cookiecheck();
        }
    };

    function rmvFav (info) {
        var index = info.getAttribute("data-index");
        favorites = JSON.parse(document.cookie.split("favoriteslist=")[1]);
        if (favorites.length === 1) {
            favorites.shift();
        } else {
            favorites.splice(index, 1);
        }
        document.cookie = "favoriteslist=" + JSON.stringify(favorites) + ";expires=Tue, 31 Dec 2030 12:00:00 GMT";
        displayFavs();
    };

    function displayPics (cb) {
        if (imglst.length === 0) {
            clearTimeout(handle);
            none.innerHTML = "There are no images with this tag.";
        } else {
            if (qry.value !== null && qry.value !== '') {
                current.innerHTML = '"' + qry.value + '"';
            } else {
                current.innerHTML = "";
            }
            none.style.display = "none";
            searchbox.style.display = "none";
            shuffle(imglst);
            for (var x = 0; x < imglst.length; x++) {

                if (imglst[x].title.length > 90) {
                    imglst[x].title = (imglst[x].title.substring(0, 90) + "...");
                }
                view.innerHTML += "<li><img id='pic-" + x + "' onclick='bgSet(this);' src='" + imglst[x].thumb + "' /><span>Description: <i>" + imglst[x].title + "</i></span><span>From: " + imglst[x].username + "</span><span>Image Source: " + imglst[x].source + "</span><span><a href=" + imglst[x].large + " target='_blank'>Full Image Link</a></span><span class='favorites'><a data-id='pic-" + x + "' data-thumb='" + imglst[x].thumb + "' data-title='" + imglst[x].title + "' data-username='" + imglst[x].username + "' data-source='" + imglst[x].source + "' data-large='" + imglst[x].large + "' onclick='addFav(this);' >Add to Favorites</a></span></li>";
            }
        }
        if (cb) {
            cb();
        }
    };

    function displayFavs () {
        imglst = '';
        view.innerHTML = '';
        favbutton.style.display = 'none';
        imglst = JSON.parse(document.cookie.split("favoriteslist=")[1]);
        if (imglst.length > 0) { current.innerHTML = "My Favorites"; }
        else {
            current.innerHTML = "No Favorites Selected.";
            setTimeout(function () {
                showsearch();
            }, 2000);
        }
        none.style.display = "none";
        searchbox.style.display = "none";
        for (var x = 0; x < imglst.length; x++) {
            if (imglst[x].title.length > 90) {
                imglst[x].title = (imglst[x].title.substring(0, 90) + "...");
            }
            view.innerHTML += "<li><img id='pic-" + x + "' onclick='bgSet(this);' src='" + imglst[x].thumb + "' /><span>Description: <i>" + imglst[x].title + "</i></span><span>From: " + imglst[x].username + "</span><span>Image Source: " + imglst[x].source + "</span><span><a href=" + imglst[x].large + " target='_blank'>Full Image Link</a></span><span class='favorites'><a data-index='" + x + "' onclick='rmvFav(this);' >Remove from Favorites</a></span></li>";
        }

    };

    function suggest_Flickr (callback) {
        processing();
        imglst = [];
        view.innerHTML = '';
        var suggestFK;
        if (XMLHttpRequest) {
            suggestFK = new XMLHttpRequest();
        } else if (typeof XDomainRequest != "undefined") {
            suggestFK = new XDomainRequest();
        }
        suggestFK.open("GET", "https://api.flickr.com/services/rest/?method=flickr.interestingness.getList&&extras=owner_name&api_key=1741068c1462d533b73748e93dd3f4a7&per_page=40&format=json&nojsoncallback=1");
        suggestFK.send(null);
        suggestFK.onreadystatechange = function () {
            if (suggestFK.readyState === 4) if (suggestFK.status === 200) {
                var sl = JSON.parse(suggestFK.response);
                for (var tl = 0; tl < sl.photos.photo.length; tl++) {
                    if (sl.photos.photo[tl].title === "") {
                        sl.photos.photo[tl].title = "(No Title Given)";
                    }
                    imglst.push({
                        thumb: "https://farm" + sl.photos.photo[tl].farm + ".staticflickr.com/" + sl.photos.photo[tl].server + "/" + sl.photos.photo[tl].id + "_" + sl.photos.photo[tl].secret + "_q.jpg",
                        large: "https://farm" + sl.photos.photo[tl].farm + ".staticflickr.com/" + sl.photos.photo[tl].server + "/" + sl.photos.photo[tl].id + "_" + sl.photos.photo[tl].secret + "_z.jpg",
                        title: sl.photos.photo[tl].title,
                        username: sl.photos.photo[tl].ownername,
                        source: "Flickr.com"
                    });
                }
                callback();
            } else {
                console.log(suggestFK.status);
            }
        };
    };


    function suggest_Pixabay (callback) {
        var request;
        var editorschoice = ['true', 'false'];
        var lng = ['id', 'cs', 'de', 'en', 'es', 'fr', 'it', 'nl', 'no', 'hu', 'ru', 'pl', 'pt', 'ro', 'fi', 'sv', 'tr', 'ja', 'ko', 'zh'];
        var ecsel = editorschoice[Math.floor(Math.random() * 2)];
        var lngsel = lng[Math.floor(Math.random() * lng.length)];
        console.log(ecsel);
        console.log(lngsel);
        if (XMLHttpRequest) {
            request = new XMLHttpRequest();
        } else if (typeof XDomainRequest != "undefined") {
            request = new XDomainRequest();
        }
        request.open("GET", "http://pixabay.com/api/?username=ryank1985&key=208479-8d54c384a5bc70942bca1de4f&per_page=40&editors_choice=" + ecsel + "&lang=" + lngsel + "", true);
        request.send(null);
        request.onreadystatechange = function () {
            if (request.readyState === 4) if (request.status === 200) {
                var data = JSON.parse(request.response);

                for (var i = 1; i < data.hits.length; i++) {
                    imglst.push({
                        thumb: data.hits[i].previewURL,
                        large: data.hits[i].webformatURL,
                        title: data.hits[i].tags,
                        username: data.hits[i].user,
                        source: "Pixabay.com"
                    });
                }
                callback();
            } else {
                console.log(request.status);
            }
        };
    };

    

    function search_Pixabay (callback) {
        if (qry.value !== null && qry.value !== "") {
            var request;
            if (XMLHttpRequest) {
                request = new XMLHttpRequest();
            } else if (typeof XDomainRequest != "undefined") {
                request = new XDomainRequest();
            }
            request.open("GET", "http://pixabay.com/api/?username=ryank1985&key=208479-8d54c384a5bc70942bca1de4f&q=" + qry.value + "&image_type=photo", true);
            request.send(null);
            request.onreadystatechange = function () {
                if (request.readyState === 4) if (request.status === 200) {
                    var data = JSON.parse(request.response);

                    for (var i = 1; i < data.hits.length; i++) {
                        imglst.push({
                            thumb: data.hits[i].previewURL,
                            large: data.hits[i].webformatURL,
                            title: data.hits[i].tags,
                            username: data.hits[i].user,
                            source: "Pixabay.com"
                        });
                    }
                    callback();
                } else {
                    console.log(request.status);
                }
            };
        }
       
    };

    function search_Flickr (callback) {
        imglst = [];
        view.innerHTML = '';
        if (qry.value !== null && qry.value !== "") {
            processing();
            var request;
            if (XMLHttpRequest) {
                request = new XMLHttpRequest();
            } else if (typeof XDomainRequest != "undefined") {
                request = new XDomainRequest();
            }
            request.open("GET", "https://api.flickr.com/services/rest/?method=flickr.photos.search&api_key=1741068c1462d533b73748e93dd3f4a7&&extras=owner_name&tags=" + qry.value + "&per_page=80&format=json&nojsoncallback=1", true);
            request.send(null);
            request.onreadystatechange = function () {
                if (request.readyState === 4) if (request.status === 200) {
                    var data = JSON.parse(request.response);

                    for (var i = 1; i < data.photos.photo.length; i++) {
                        if (data.photos.photo[i].title === "") {
                            data.photos.photo[i].title = "(No Title Given)";
                        }
                        imglst.push({
                            thumb: "https://farm" + data.photos.photo[i].farm + ".staticflickr.com/" + data.photos.photo[i].server + "/" + data.photos.photo[i].id + "_" + data.photos.photo[i].secret + "_q.jpg",
                            large: "https://farm" + data.photos.photo[i].farm + ".staticflickr.com/" + data.photos.photo[i].server + "/" + data.photos.photo[i].id + "_" + data.photos.photo[i].secret + "_z.jpg",
                            title: data.photos.photo[i].title,
                            username: data.photos.photo[i].ownername,
                            source: "Flickr.com"
                        });
                    }
                   callback();
                } else {
                    console.log(request.status);
                }
            };
        }
    };

    function runSearches() {
        search_Flickr(function(){
            search_Pixabay(function() {
                displayPics();
            });
        });
    }

    function runSuggestions() {
        suggest_Flickr(function(){
            suggest_Pixabay(function() {
                displayPics();
            });
        });
    }

    

    function hideoly () {
        overlay.style.display = "none";
    };

    function showsearch () {
        cookiecheck();
        if (searchbox.style.display === "block" && searchbox.style.zIndex === "1") {
            searchbox.style.display = "none";
            searchbox.style.zIndex = "-1";
        }
        if (loginbox.style.display === "block" && loginbox.style.zIndex === "1") {
            loginbox.style.display = "none";
            loginbox.style.zIndex = "-1";
            searchbox.style.display = "block";
            searchbox.style.zIndex = "1";
            qry.focus();
        }
         else {
            qry.value = '';
            none.style.display = "none";
            overlay.style.display = "none";
            searchbox.style.display = "block";
            searchbox.style.zIndex = "1";
            qry.focus();
        }
    };

    function showlogin () {
        if (searchbox.style.display === "block") {
            searchbox.style.display = "none";
            searchbox.style.zIndex = "-1";
        }
        if (loginbox.style.display === "block" && loginbox.style.zIndex === "1") {
            loginbox.style.display = "none";
            loginbox.style.zIndex = "-1";
        } else {
            none.style.display = "none";
            overlay.style.display = "none";
            loginbox.style.display = "block";
            loginbox.style.zIndex = "1";
        }
    };

    function getLoginInfo () {
        user = $("user");
        pass = $("pass");
        if (user.value !== "" && pass.value !== "") {
            logreq = new XMLHttpRequest();
            logreq.open('GET', 'login.php?user=' + user.value + '&pass=' + pass.value + '', true);
            logreq.onreadystatechange = function () {
                if (this.readyState === 4) {
                    if (this.status >= 200 && this.status < 400) {
                        // Success!
                        $("testresponse").innerHTML = "Excellent, your favorites were retrieved.";
                    } else {
                        // Error :(
                    }
                }
            };
            logreq.send();
            logreq = null;
        }
    }

    function searchKeyPress (e) {
        var key = (e.keyCode ? e.keyCode : e.which);
        if (searchbox.style.display === 'block' || loginbox.style.display === 'none') {
            if (key === 13) {
                runSearches();
            }
        }
        if (searchbox.style.display === 'none') {
            if (key === 70) {
                setTimeout(
             function () {
                 showsearch();
             }, 100);
            }
        }
        if (loginbox.style.display === 'block') {
            if (key === 13) {
                setTimeout(
             function () {
                 getLoginInfo();
             }, 100);
            }
        }
        if (searchbox.style.display === 'block') {
            if (key === 67 && e.ctrlKey) {
                setTimeout(
             function () {
                 searchbox.style.display = "none";
             }, 100);
            }
        }
    };

    function bgSet (whut) {
        var bgi = whut.id;
        var index = [];
        index = bgi.split("pic-");
        overlay.style.backgroundImage = "url('" + imglst[index[1]].large + "')";
        overlay.style.display = "block";
        overlay.style.zIndex = "1";
    };

    window.onload = function () {
        favrequest = $("favrequest");
        favbutton = $("favbutton");
        qry = $("srcqry");
        searchbox = $("searchbox");
        loginbutton = $("login");
        loginsearchbutton = $("login-search");
        loginbox = $("loginbox");
        signin = $("signin");
        view = $("view");
        getval = $("getval");
        overlay = $("overlay");
        request = $("request");
        none = $("none");
        closeit = document.getElementsByClassName("closeit");
        insp = $("insp");
        current = $("current");
        insp.onclick = runSuggestions;
        closeit[0].onclick = showsearch;
        closeit[1].onclick = showlogin;
        getval.onclick = showsearch;
        loginbutton.onclick = showlogin;
        loginsearchbutton.onclick = showlogin;
        signin.onclick = getLoginInfo;
        request.onclick = runSearches;
        overlay.onclick = hideoly;
        favrequest.onclick = displayFavs;
        favbutton.onclick = displayFavs;
        showsearch();
        setText();
        cookiecheck();
        window.onresize = setText;
        document.onkeydown = searchKeyPress;
    };
    